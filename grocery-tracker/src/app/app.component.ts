import { Component } from '@angular/core';
import { ListService } from './services/list.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'grocery-tracker';

  constructor(private listService:ListService){
    this.listService.loadListsFromStorage();
  }

}
