import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from './material.module';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { WelcomePageComponent } from './pages/welcome-page/welcome-page.component';
import { CreatePageComponent } from './pages/create-page/create-page.component';
import { SearchPageComponent } from './pages/search-page/search-page.component';
import { RemovePageComponent } from './pages/remove-page/remove-page.component';
import { NewListComponent } from './components/new-list/new-list.component';
import { ListsPageComponent } from './pages/lists-page/lists-page.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ListComponent } from './components/list/list.component';
import { ListItemComponent } from './components/list-item/list-item.component';
import { NewItemComponent } from './components/new-item/new-item.component';
import { AutoWidthDirective } from './directives/auto-width.directive';
import { TrackerInfoComponent } from './components/tracker-info/tracker-info.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    WelcomePageComponent,
    CreatePageComponent,
    SearchPageComponent,
    RemovePageComponent,
    NewListComponent,
    ListsPageComponent,
    ListComponent,
    ListItemComponent,
    NewItemComponent,
    AutoWidthDirective,
    TrackerInfoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
