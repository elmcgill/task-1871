import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, throwError, BehaviorSubject, catchError } from 'rxjs';
import { Item, List, Tracker } from '../models/List';

interface ListArgs {
  trackerId: string;
  name: string;
}

interface NewItemArgs {
  item:Item;
  trackerId:string;
  listId: string;
}

@Injectable({
  providedIn: 'root'
})
export class ListService {

  private characters ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  private trackers:Tracker[] = [];

  trackerSubject = new BehaviorSubject(this.trackers);

  constructor(private router:Router) {};

  private generateId():string{
    let id = "";
    for(let i=0; i<10; i++){
      id += this.characters.charAt(Math.floor(Math.random() * this.characters.length));
    }
    return id;
  }

  loadListsFromStorage(): void {
    let lists = localStorage.getItem("lists");

    if(lists){
      this.trackers = JSON.parse(lists);
      this.trackerSubject.next(this.trackers);
    }
  }

  createList(args:ListArgs): Observable<Tracker> {
    return new Observable<Tracker>((observer) => {
      if(!args.trackerId){
        let id = this.generateId();
        let listId = this.generateId();
        let tracker = {
          id,
          owner: "",
          lists:[{
            id: listId,
            name:args.name,
            items:[]
          }]
        }

        this.trackers.push(tracker);

        localStorage.setItem("lists", JSON.stringify(this.trackers));

        this.trackerSubject.next(this.trackers);
        observer.next(tracker);
        this.router.navigateByUrl(`/lists/${id}`);
      } else {
        let tracker:Tracker | undefined;

        this.trackers.forEach((cur, index) => {
          if(args.trackerId === cur.id){
            tracker =  cur;
            this.trackers.splice(index, 1);
          }
        });

        if(tracker){
          let listId = this.generateId();
          tracker.lists.push({
            id: listId,
            name:args.name,
            items:[]
          });

          this.trackers.push(tracker);
          localStorage.setItem("lists", JSON.stringify(this.trackers));
          this.trackerSubject.next(this.trackers);
          observer.next(tracker);
          this.router.navigateByUrl(`/lists/${tracker.id}`);
        }else{
          observer.error('Tracker doesnt exist');
        }
      }
    });
  }

  changeOwner(args:Tracker) {
    let modified = this.trackers.filter((cur) => cur.id !== args.id);
    modified.push(args);
    localStorage.setItem("lists", JSON.stringify(this.trackers));
    this.trackerSubject.next(this.trackers);
  }

  addItem(args:NewItemArgs): void {
    let itemId = this.generateId();
    let item = args.item;
    item.id = itemId;

    let t:Tracker = {
      id: '',
      owner: '',
      lists: []
    };

    let l:List = {
      id: '',
      name: '',
      items: []
    };

    this.trackers.forEach((cur, index) => {
      if(cur.id === args.trackerId){
        t = cur;
        this.trackers.splice(index, 1);
      }
    });

    t.lists.forEach((cur, index) => {
      if(cur.id === args.listId){
        l = cur;
        t.lists.splice(index, 1);
      }
    });

    l.items.push(item);
    t.lists.push(l);

    this.trackers.push(t);

    this.trackerSubject.next(this.trackers);
    localStorage.setItem("lists", JSON.stringify(this.trackers));
  }

  removeItem(args:NewItemArgs):void {
    let t:Tracker = {
      id: '',
      owner: '',
      lists: []
    };

    let l:List = {
      id: '',
      name: '',
      items: []
    };

    this.trackers.forEach((cur, index) => {
      if(cur.id === args.trackerId){
        t = cur;
        this.trackers.splice(index, 1);
      }
    });

    t.lists.forEach((cur, index) => {
      if(cur.id === args.listId){
        l = cur;
        t.lists.splice(index, 1);
      }
    });

    l.items = l.items.filter((i) => i.id !== args.item.id);

    t.lists.push(l);

    this.trackers.push(t);
    this.trackerSubject.next(this.trackers);
    localStorage.setItem("lists", JSON.stringify(this.trackers));
  }

  searchList(id:string): Tracker | undefined {
    for(let i=0; i<this.trackers.length; i++){
      if(this.trackers[i].id == id){
        return this.trackers[i];
      }
    }
    return undefined;
  }

  removeList(id:string):boolean {
    let found:boolean=false;

    let updated = this.trackers.filter((cur) => {
      if(cur.id === id){
        found = true;
      }else{
        return cur;
      }
    });

    this.trackers = updated;

    this.trackerSubject.next(this.trackers);

    localStorage.setItem("lists", JSON.stringify(this.trackers));

    return found;
  }
}
