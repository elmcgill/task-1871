import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreatePageComponent } from './pages/create-page/create-page.component';
import { ListsPageComponent } from './pages/lists-page/lists-page.component';
import { RemovePageComponent } from './pages/remove-page/remove-page.component';
import { SearchPageComponent } from './pages/search-page/search-page.component';
import { WelcomePageComponent } from './pages/welcome-page/welcome-page.component';

const routes: Routes = 
  [
    {path: '', component: WelcomePageComponent, pathMatch:'full'},
    {path: 'create', component: CreatePageComponent},
    {path: 'remove', component: RemovePageComponent},
    {path: 'search', component: SearchPageComponent},
    {path: 'lists/:id', component:ListsPageComponent}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
