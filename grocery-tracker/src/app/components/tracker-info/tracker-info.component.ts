import { Component, Input, OnInit } from '@angular/core';
import { Tracker } from 'src/app/models/List';

@Component({
  selector: 'tracker-info',
  templateUrl: './tracker-info.component.html',
  styleUrls: ['./tracker-info.component.css']
})
export class TrackerInfoComponent implements OnInit {

  @Input() tracker:Tracker;
  link:string;

  constructor() {
    this.tracker = {
      id:"",
      owner:"",
      lists: []
    }
    this.link = "";
  }

  ngOnInit(): void {
    this.tracker.owner = this.tracker.owner || "Unknown Guest";
    this.link = `/lists/${this.tracker.id}`;
  }

}
