import { Component, OnInit } from '@angular/core';
import {FormGroup, FormControl} from '@angular/forms';
import { ListService } from 'src/app/services/list.service';
import { Router } from '@angular/router';
import { Tracker } from 'src/app/models/List';
import { catchError, throwError } from 'rxjs';

@Component({
  selector: 'new-list',
  templateUrl: './new-list.component.html',
  styleUrls: ['./new-list.component.css']
})
export class NewListComponent implements OnInit {

  listForm = new FormGroup({
    id: new FormControl<string>(''),
    name: new FormControl<string>('')
  });

  error:boolean;

  tracker:Tracker | undefined;

  constructor(private listService:ListService, private router:Router) {
    this.error = false;
   }

  ngOnInit(): void {
  }

  onSubmit(): void {
    let id = "";
    if(this.listForm.value.id) id = this.listForm.value.id;

    if(this.listForm.value.name){
      this.listService.createList({
        trackerId: id,
        name: this.listForm.value.name
      }).pipe(
        catchError(err => {
          console.log("error");
          this.error = true;
          return throwError(() => new Error('no lists'))})
      ).subscribe(data => this.tracker = data);


      if(!this.error){
        this.router.navigateByUrl("/lists/" + this.tracker?.id);
      }
      
    }

    
  }

}
