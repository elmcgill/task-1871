import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { Item } from 'src/app/models/List';

@Component({
  selector: 'new-item',
  templateUrl: './new-item.component.html',
  styleUrls: ['./new-item.component.css']
})
export class NewItemComponent implements OnInit {

  @Output() onAddItem: EventEmitter<Item> = new EventEmitter();

  name:string;
  price:number;
  error: boolean;

  constructor() { 
    this.name = '';
    this.price = 0.0;
    this.error = false;
  }

  ngOnInit(): void {
  }

  addItem(): void {
    if(!this.name || !this.price){
      this.error = true;
      return;
    }

    const newItem:Item = {
      id: "",
      name: this.name,
      price: this.price
    }

    this.onAddItem.emit(newItem);

    this.error = false;
    this.name = '';
    this.price = 0.0;
  }

}
