import { Component, Input, OnInit } from '@angular/core';
import { Item, List } from 'src/app/models/List';
import { ListService } from 'src/app/services/list.service';

@Component({
  selector: 'list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {

  @Input() trackerId:string = '';

  @Input() list: List = {
    id: '',
    name: '',
    items: []
  }

  addToggle:boolean;

  total:number;

  constructor(private listService: ListService) { 
    this.addToggle = false;
    this.total = 0.0;
  }

  ngOnInit(): void {
    this.list.items.forEach((item) => {
      this.total += item.price;
    })
  }

  addItem(item:Item): void {
    //create a method in the list service
    console.log(item);
    this.listService.addItem({
      trackerId: this.trackerId,
      listId: this.list.id,
      item: item
    });
    this.toggleAdd();
    this.total = this.total + item.price;
  }

  removeItem(item:Item):void{
    console.log("Remove Item: ", item);
    this.listService.removeItem({
      trackerId: this.trackerId,
      listId: this.list.id,
      item: item
    });

    this.total = this.total - item.price;
  }

  toggleAdd(): void {
    this.addToggle = !this.addToggle;
  }

}
