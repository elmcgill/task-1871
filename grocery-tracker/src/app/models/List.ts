export interface Item {
    id: string;
    name: string;
    price: number;
}

export interface List {
    id: string;
    name:string;
    items: Item[];
}

export interface Tracker{
    id: string;
    owner: string;
    lists: List[]
}