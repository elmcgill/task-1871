import { Component, OnInit } from '@angular/core';
import { Tracker } from 'src/app/models/List';
import { ListService } from 'src/app/services/list.service';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.css']
})
export class SearchPageComponent implements OnInit {

  id: string;
  searched: boolean;
  found: boolean;
  tracker:Tracker;

  constructor(private listService:ListService) {
    this.id = "";
    this.searched=false;
    this.found=false;
    this.tracker={
      id: "",
      owner: "",
      lists: []
    };
  }

  ngOnInit(): void {
  }

  search():void{
    this.searched = true;
    let t:Tracker | undefined = this.listService.searchList(this.id);

    console.log("t:", t);

    if(!t){
      this.found = false;
      return;
    }

    this.tracker = t;
    this.found = true;
  }

}
