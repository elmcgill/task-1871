import { Component, OnInit } from '@angular/core';
import { ListService } from 'src/app/services/list.service';

@Component({
  selector: 'app-remove-page',
  templateUrl: './remove-page.component.html',
  styleUrls: ['./remove-page.component.css']
})
export class RemovePageComponent implements OnInit {

  id:string;
  deleted:boolean;
  searched:boolean;

  constructor(private listService:ListService) {
    this.id = "";
    this.deleted = false;
    this.searched = false;

  }

  ngOnInit(): void {
  }

  delete():void{
    this.searched = true;
    this.deleted = this.listService.removeList(this.id);
  }

}
