import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router'
import { Tracker } from 'src/app/models/List';
import { ListService } from 'src/app/services/list.service';

@Component({
  selector: 'lists-page',
  templateUrl: './lists-page.component.html',
  styleUrls: ['./lists-page.component.css']
})
export class ListsPageComponent implements OnInit {

  id: string;
  tracker:Tracker;
  user:string;
  editing:boolean;

  constructor(private route: ActivatedRoute, private listService: ListService, private router: Router) { 
    this.user = '';
    this.id = '';
    this.editing = false;
    this.tracker = {
      id:'',
      owner: '',
      lists: []    
    }
  }

  ngOnInit(): void {

    this.route.paramMap.subscribe((params:ParamMap) => {
      let pId = params.get('id');
      if(pId !== null){

        this.listService.trackerSubject.subscribe(data => {
          data.forEach((val) => {
            if(val.id === pId){
              this.tracker = val;
            }
          })
        })

        if(this.tracker.id){
          this.id = pId;
          this.user = this.tracker.owner || 'Guest';
        } else {
          this.router.navigateByUrl("/");
        }
        
      }
    })
  }

  toggleEditOwner():void {
    this.editing = true;
  }

  updateUser():void {
    if(this.tracker){
      this.tracker.owner = this.user;
      this.listService.changeOwner(this.tracker);
    }

    this.editing = false;
  }

}
