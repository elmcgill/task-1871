# Grocery List Tracker

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 14.2.1.

## Description

Grocery List Tracker is an application which utilizes Angular 4+ and the browsers local storage to keep track of sets of grocery lists for the user. Upon creating a new set of lists, the user is given a `Tracker ID` which can be used to add new lists to a set, search for the set, and remove the set from persistence. The purpose is to allow users to keep track of all their grocery lists in a single place.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The application will automatically reload if you change any of the source files.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Live Version

https://grocery-lists-tracker.s3.amazonaws.com/index.html

# Application User Stories

## User Types

New User
- A user who has yet to create a set of grocery lists to be tracked

List Owner
- A user who has created at least one grocery list to be tracked and managed

## 1. As a new user, I should be able to make a new set of lists, so that I can keep track of my grocery items
### Acceptance Criteria
- [ ] On the `/create` page, there should be a form with two input fields and a submit button
- [ ] The `Create New List` button should be disabled if no value is present in the `List Name` field
- [ ] The `Create New List` button should NOT change colors to orange on hover if no value is present in the `List Name` field
- [ ] The `Create New List` button should be enabled if a value is present in the `List Name` field
- [ ] The `Create New List` button should change colors to orange when hovered if there is a value present in the `List Name` field
- [ ] The application should navigate to the `/lists/{TrackerID}` page when the form is submited with a valid `List Name` value AND no `Tracker ID` value

## 2. As a list owner, I should be able to add a list to an existing set of lists, so I can have multiple grocery lists
### Acceptance Criteria
- [ ] The `Tracker ID` field of the `/create` page should allow returning users to input an existing ID
- [ ] The `Create New List` button should be disabled if `Tracker ID` has a value, but `List Name` has no value
- [ ] The `Create New List` button should NOT change colors to orange if `Tracker ID` has a value, but `List Name` has no value
- [ ] The application should navigate to the `/lists/{TrackerID}` page when the form is submitted with a valid `Tracker ID` value AND a valid `List Name` value
- [ ] The `/create` page should display an error message if the form is submitted with an invalid `Tracker ID` value
- [ ] The invalid Tracker ID error message should appear in red text to be easily noticible


## 3. As a list owner, I should be able to see all lists associated with a Tracker, so I can manage multiple lists at a time

### Acceptance Criteria
- [ ] The `/lists/{TrackerID}` page should display all grocery lists associated with at Tracker ID

## 4. As a list owner, I should be able to view all items on my grocery list, so I can keep track of my unbought items
### Acceptance Criteria
- [ ] Each list on the `/lists/{TrackerID}` should contain every expected item
- [ ] Each item in the list should contain an `Item name`, `Item price` and a `Remove Item` button
- [ ] Each list should have a scroll bar if more than three items are placed in them

## 5. As a list owner, I should be able to add new grocery items to a list, so I can keep track of any additional items I may need
### Acceptance Criteria
- [ ] Each list should have an `Add New Item` button
- [ ] Clicking on an `Add New Item` button should display a new item form
- [ ] The new item form should have input fields for `Item Name` and `Item Price` and an `Add Item` button
- [ ] The `Add Item` button should be disabled if no values are present for `Item Name` AND `Item Price`
- [ ] The `Add Item` button should NOT change color to orange on hover if no values are present for `Item Name` AND `Item Price`
- [ ] The `Add Item` button should be enabled only after both values are present in `Item Name` AND `Item Price`
- [ ] The `Add Item` button should change color to orange on hover if both values are present in `Item Name` AND `Item Price`
- [ ] Submitting the new item form should result in the inputted item displaying on the appropriate list

## 6. As a list owner, I should be able to remove a purchased grocery item from a list, so I can keep track of only the items I still need to buy
### Acceptance Criteria
- [ ] Each grocery item in a list should have a `Remove Item` button
- [ ] Clicking on the `Remove Item` button should remove the specified item from the appropriate list

## 7. As a list owner, I should be able to view the total cost of a list, so that I can know how much I will spend at the store
- [ ] Each list should have a `List Total`
- [ ] When items are added to the list, the `List Total` should be recalculated to include the new items price
- [ ] When items are removed from the list, the `List Total` should be recalculated to exclude the old items price

## 8. As a list owner, I should be able to delete a list, so I can track only the current lists I need
### Acceptance Criteria
- [ ] Each list should have a `Delete List` button
- [ ] When the `Delete List` button is pressed the appropriate list should be removed from the `/lists/{TrackerID}` page

## 9. As a list owner, I should be able to search for a set of lists, so I can mantain multiple list sets
### Acceptance Criteria
- [ ] On the `/search` page, there should be an input for `Tracker ID` and a `Search` button
- [ ] The `Search` button should be disabled if no value is present in the `Tracker ID` field
- [ ] The `Search` button should NOT change colors to blue on hover when no value is present in the `Tracker ID` field
- [ ] The `Search` button should be enabled when a value is present in the `Tracker ID` field
- [ ] The `Search` button should change colors to blue on hover when a value is present in the `Tracker ID` field
- [ ] The `/search` page should display the information about the submitted grocery list tracker when the form is submitted with a valid `Tracker ID`
- [ ] The `/search` page should display the `List Set ID`, `List Owner`, `Number of Lists` and a `Link to List` when a valid grocery lists tracker is found
- [ ] The `Link to List` should change color to orange and become underlined when hovered over
- [ ] Clicking on the `Link to List` text should result in application navigating to `/lists/{TrackerId}`
- [ ] The `/search` page should display an error message if the form was submitted with an invalid `Tracker ID`
- [ ] The invalid `Tracker ID` error message should be displayed in red text to be easily noticible

## 10. As a list owner, I should be able to delete a set of lists, so I can keep only the sets I need
### Acceptance Criteria
- [ ] On the `/remove` page, there should be an input for `Tracker ID` and a `Remove` button
- [ ] The `Remove` button should be disabled if no value is present in the `Tracker ID` field
- [ ] The `Remove` button should NOT change colors to yellow on hover when no value is present in the `Tracker ID` field
- [ ] The `Remove` button should be enabled when a value is present in the `Tracker ID` field
- [ ] The `Remove` button should change colors to yellow on hover when a value is present in the `Tracker ID` field
- [ ] The `/remove` page should display a success message when the form is submitted with a valid `Tracker ID`
- [ ] The `/remove` page should display an error message when the form is submitted with an invalid `Tracker ID`
- [ ] The invalid `Tracker ID` error message should be displayed in red text to be easily noticible

## 11. As a new user, I should be able to navigate between pages, so I can manage create, search, or delete sets of grocery lists
### Acceptance Criteria
- [ ] The application should have a navigate bar with three options, `Create New List`, `Find List Set by ID`, and `Remove List Set`
- [ ] The navigation bar background should should change color to blue on hover behind the current selection
- [ ] On the `/` page the `Start by creating a new list...` that when clicked, should navigate the application to the `/create` page
- [ ] When the `Create New List` text on the navigation bar is clicked, the application should navigate to the `/create` page
- [ ] When the `Find List Set by ID` text on the navigation bar is clicked, the application should navigate to the `/search` page
- [ ] When the `Remove List Set` text on the navigation bar is clicked, the application should navigate to the `/remove` page

## 12. As a list owner, I should be able to change my display name, so the application can remember my name
### Acceptance Criteria
- [ ] On the `/lists/{TrackerID}` page, there should be a welcome message which includes `Welcome {Name}`
- [ ] The welcome message `Name` should default to `Guest`
- [ ] The welcome message should include a pencil icon after the `Name`
- [ ] The pencil icon should change colors to orange on hover
- [ ] When the pencil icon is clicked, the `Name` value should be able to be changed by typing in a new `Name`
- [ ] When editing the display name, the pencil icon should change to a green checkbox icon
- [ ] The checkbox colors should be inverted when hovered over
- [ ] Clicking on the checkbox should save the display name on the grocery list set
- [ ] The display name should not be saved if left empty
- [ ] After changing the display name, it should be saved through refreshing the application
